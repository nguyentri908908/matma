from flask import Flask, render_template, request
import math
import random
import string

app = Flask(__name__)

main = string.ascii_lowercase
def gcd(a, b):
    while b:
        a, b = b, a % b
    return a
def multiplicative_inverse(a, m):
    a = a % m
    for x in range(1, m):
        if ((a * x) % m == 1):
            return x
    return 1

def generate_keypair(p, q):
    n = p * q
    phi = (p - 1) * (q - 1)

    e = random.randint(2, phi - 1)  # Đảm bảo e khác 1 và nguyên tố cùng nhau với phi(n)
    while gcd(e, phi) != 1:
        e = random.randint(2, phi - 1)

    d = multiplicative_inverse(e, phi)

    return ((e, n), (d, n))


def encrypt(public_key, to_encrypt):
    key, n = public_key
    cipher = pow(to_encrypt, key, n)
    return cipher

def decrypt(private_key, to_decrypt):
    key, n = private_key
    decrypted = pow(to_decrypt, key, n)
    return decrypted

@app.route('/', methods=['GET', 'POST'])
def index():
    # Trong hàm index() của ứng dụng Flask
    # Trong hàm index() của ứng dụng Flask
    if request.method == 'POST':
        p = int(request.form['p'])
        q = int(request.form['q'])
        public, private = generate_keypair(p, q)
        message = request.form['message']
        message = message.replace(" ", "")
        message = message.lower()
        arr = []
        cipher_text = []
        encrypted_message = []  # Thêm danh sách để lưu trữ văn bản đã được mã hóa
        for i in message:
            if i in main:
                arr.append(main.index(i))
            else:
                # Xử lý các ký tự không nằm trong `main`
                arr.append(-1)  # Thêm một giá trị đặc biệt để biểu thị ký tự này
        for i in arr:
            if i != -1:
                cipher_text.append(encrypt(public, i))
                # Chuyển đổi về ký tự và thêm vào danh sách văn bản đã được mã hóa
                encrypted_message.append(main[i])
            else:
                # Nếu là ký tự không nằm trong `main`, giữ nguyên
                cipher_text.append(i)
                encrypted_message.append(i)
        plain = []
        for i in cipher_text:
            if i != -1:
                plain.append(decrypt(private, i))
            else:
                # Nếu là ký tự không nằm trong `main`, giữ nguyên
                plain.append(i)
        plain_text = ''
        for i in plain:
            if i != -1:
                plain_text = plain_text + main[i]
            else:
                # Nếu là ký tự không nằm trong `main`, giữ nguyên
                plain_text = plain_text + i
        return render_template('result.html', public_key=public, private_key=private, cipher_text=cipher_text, plain_text=plain_text, encrypted_message=encrypted_message)

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
